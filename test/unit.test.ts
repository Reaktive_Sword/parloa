import {Calculator} from '../src/Calculator';
import { suite, test } from '@testdeck/mocha';
import * as _chai from 'chai';
import { expect } from 'chai';
import { customer } from '../src/main';

_chai.should();
_chai.expect;

@suite class UnitTests {
  private calculator: Calculator;
  private mainCompany: customer;
  private customerTrueA: customer;
  private customerTrueB: customer;
  private customerFalse: customer;
  private customers: customer[] = [];

  before() {
    this.mainCompany = {
        id: "Test",
        lat: 55.49146761091263,
        long: 8.4109969788181
      };
      this.customerFalse = {
        id: "CustomerFalse",
        lat: 55.416638785673044,
        long: 10.402586710898161
      };
      this.customerTrueA = {
        id: "CustomerTrueA",
        lat: 55.523662351812156,
        long: 8.35651448637173
      };
      this.customerTrueB = {
        id: "CustomerTrueB",
        lat: 55.523662351812156,
        long: 8.35651448637173
      };

      this.customers.push(this.customerTrueB);
      this.customers.push(this.customerTrueA);
      this.customers.push(this.customerFalse);
      this.calculator = new Calculator(this.mainCompany, this.customers);
  }

  @test 'Solve_Customers_Customer' () {
      var test = this.calculator.Solve();
      expect(test[0]).equal("CustomerTrueA");
  }

  @test 'Solve_Customers_Count' () {
    var test = this.calculator.Solve();
    expect(test.length).equal(2);
}

  @test 'GreatCircleDistance_CustomerOutsideArea_Distance' () {
    var test: number = this.calculator.GreatCircleDistance(this.mainCompany, this.customerFalse);
    expect(test).equal(125.85062790470637);
  }

  @test 'GreatCircleDistance_CustomerInsideArea_Distance' () {
    var test: number = this.calculator.GreatCircleDistance(this.mainCompany, this.customerTrueA);
    expect(test).equal(4.958379793180477);
  }
}