import fs from 'fs';
import { Calculator } from './Calculator';

export type customer = {
  id: string;
  lat: number;
  long: number;
};

main();

function main() {
  const parloa: customer = {
    id: "Parloa",
    lat: 52.493256,
    long: 13.446082
  };

  var customers: customer[];
  customers = readFromFile();
  var calculator = new Calculator(parloa, customers);
  var result = calculator.Solve();

  result.forEach(element => {
    console.log(element);
  });
}

function readFromFile(): customer[] {
  var result: customer[] = [];
  try {
    
    const data = fs.readFileSync('src/customers_Bulat.txt', 'utf8');

    var strs: string[] = data.split(/\r?\n/);
    strs.forEach(element => {
      const customerStr: string[] = element.split(/[, :]+/);
      const cust: customer = {
        id: customerStr[1],
        lat: +customerStr[3],
        long: +customerStr[5]
      };
      result.push(cust);
    });
    return result;
  } catch (error) {
    console.error(error);
    return result;
  }
  
}


