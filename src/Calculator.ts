import { customer } from "./main";

export class Calculator {
    public mainCompany: customer;
    public customers: customer[];
    private readonly DISTANSE: number = 100;

    constructor(mainCompany: customer, customers: customer[]) {
        this.mainCompany = mainCompany;
        this.customers = customers;
      }

    
      public Solve(): string[] {
        var result: string[] = [];
        try {
          this.customers.forEach(element => {
            var distance = this.GreatCircleDistance(this.mainCompany, element);
            if (distance <= this.DISTANSE) result.push(element.id);
          });
          return result.sort();
        } catch (error) {
          console.error(error);
          return result;
        }
      }
      
      public GreatCircleDistance(c1: customer, c2: customer): number {
      
        const R = 6371;
        var dLat = this.toRad(c2.lat-c1.lat);
        var dLon = this.toRad(c2.long-c1.long);
        var lat1 = this.toRad(c1.lat);
        var lat2 = this.toRad(c2.lat);
      
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
          Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c;
        
        return Math.abs(d);
      }
      
      private toRad(value: number): number{
        return value * Math.PI / 180;
      }
      
  }